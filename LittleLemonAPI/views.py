import datetime

from django.contrib.auth.models import User, Group
from django.shortcuts import render, get_object_or_404
from djoser.serializers import UserSerializer
from rest_framework import generics, permissions, response, status, viewsets
from rest_framework.decorators import permission_classes
from rest_framework.generics import UpdateAPIView

from .models import Category, MenuItem, Cart, Order, OrderItem
from .serializers import CategorySerializer, MenuItemSerializer, CartSerializer, OrderSerializer, \
    UserWithGroupsSerializer, OrderItemSerializer


class ManagersOnlyPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.is_authenticated and request.user.groups.filter(name='Manager').exists()


class ManagersAndGetPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.is_authenticated and (request.user.groups.filter(name='Manager').exists() or request.method == "GET")

class ManagersDeliveryAndGetPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.is_authenticated and (request.user.groups.filter(name='Manager').exists() or (request.user.groups.filter(name='Delivery Crew').exists() and request.method == "PATCH") or request.method == "GET")


class CustomerPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.is_authenticated and (not request.user.groups.filter(name='Manager').exists() or not request.user.groups.filter(name='Delivery Crew').exists())


# Create your views here.
class CategoryView(generics.ListCreateAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    permission_classes = [ManagersAndGetPermission]


class MenuItemsView(generics.ListCreateAPIView):
    queryset = MenuItem.objects.all()
    serializer_class = MenuItemSerializer
    permission_classes = [ManagersAndGetPermission]


class MenuItemView(generics.RetrieveUpdateAPIView):
    queryset = MenuItem.objects.all()
    serializer_class = MenuItemSerializer
    permission_classes = [ManagersAndGetPermission]


class CartView(generics.ListCreateAPIView):
    queryset = Cart.objects.all()
    serializer_class = CartSerializer
    permission_classes = [CustomerPermission]

    def get_queryset(self):
        queryset = super().get_queryset()

        # Filter the queryset to only include carts for the logged-in user.
        if self.request.user.is_authenticated:
            queryset = queryset.filter(user=self.request.user)

        return queryset

    def create(self, request, *args, **kwargs):
        # Get the menu item and quantity from the request data.
        menu_item_id = request.data.get('menuitem')
        quantity = int(request.data.get('quantity'))

        menu_item = MenuItem.objects.get(pk=menu_item_id)

        # Create a new cart item if one does not already exist for the menu item.
        cart_item, created = Cart.objects.get_or_create(
            user=request.user,
            menuitem_id=menu_item_id,
            quantity=quantity,
            unit_price=menu_item.price,
            price=menu_item.price * quantity
        )

        # Save the cart item.
        cart_item.save()

        # Serialize the cart item and return it in the response.
        serializer = CartSerializer(cart_item)
        return response.Response(serializer.data, status=status.HTTP_201_CREATED)

    def delete(self, request, *args, **kwargs):
        # Get the cart object for the logged-in user.
        # cart = Cart.objects.get(user=self.request.user)
        carts = Cart.objects.filter(user=self.request.user)

        # Delete all the menu items in the cart.
        carts.delete()

        # Return a response indicating that the cart was successfully deleted.
        return response.Response(status=status.HTTP_200_OK)



class OrdersView(generics.ListCreateAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permission_classes = [CustomerPermission]

    def get_queryset(self):
        queryset = super().get_queryset()

        # Filter the queryset to only include carts for the logged-in user.
        if self.request.user.groups.filter(name = "Manager").exists():
            queryset = queryset.all()
        elif self.request.user.groups.filter(name = "Delivery Crew").exists():
            queryset = queryset.filter(delivery_crew=self.request.user)
        elif self.request.user.is_authenticated:
            queryset = queryset.filter(user=self.request.user)

        return queryset

    def create(self, request, *args, **kwargs):
        # Get the menu item and quantity from the request data.
        carts = Cart.objects.filter(user=request.user)
        total = 0

        for cart in carts:
            total += cart.price

        # Create a new order if one does not already exist for the menu item.
        order, created = Order.objects.get_or_create(
            user=request.user,
            total=total,
            date=datetime.datetime.now()
        )

        order.save()

        for cart in carts:
            # Create a new cart item if one does not already exist for the menu item.
            order_item, created = OrderItem.objects.get_or_create(
                order=order,
                menuitem_id=cart.menuitem.id,
                quantity=cart.quantity,
                unit_price=cart.menuitem.price,
                price=cart.price
            )

            order_item.save()

        carts.delete()

        return response.Response({"message": "Order Created."}, status=status.HTTP_201_CREATED)

class OrderView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permission_classes = [ManagersDeliveryAndGetPermission]

class OrderItemView(generics.ListCreateAPIView):
    queryset = OrderItem.objects.all()
    serializer_class = OrderItemSerializer
    permission_classes = [CustomerPermission]

    def get_queryset(self):
        queryset = super().get_queryset()

        # Filter the queryset to only include carts for the logged-in user.
        if self.request.user.is_authenticated:
            queryset = queryset.filter(user=self.request.user)

        return queryset


class GroupManagersView(generics.ListAPIView, generics.UpdateAPIView):
    queryset = User.objects.filter(groups__name="Manager")
    serializer_class = UserWithGroupsSerializer
    permission_classes = [ManagersOnlyPermission]

    def update(self, request, *args, **kwargs):
        username = request.data["username"]
        if username:
            user = get_object_or_404(User, username=username)
            managers = Group.objects.get(name="Manager")
            managers.user_set.add(user)

            return response.Response({"message": "User added to Manager group."}, status.HTTP_201_CREATED)

        return response.Response({"message": "Failed to add User to Manager group."}, status.HTTP_400_BAD_REQUEST)


class GroupManagerView(generics.RetrieveDestroyAPIView):
    queryset = User.objects.filter(groups__name="Manager")
    serializer_class = UserWithGroupsSerializer
    permission_classes = [ManagersOnlyPermission]

    def delete(self, request, pk=None, *args, **kwargs):
        if pk:
            user = get_object_or_404(User, id=pk, groups__name="Manager")
            managers = Group.objects.get(name="Manager")
            managers.user_set.remove(user)

            return response.Response({"message": "User removed from Manager group."})

        return response.Response({"message": "Failed to remove User from Manager group."}, status.HTTP_400_BAD_REQUEST)


class GroupDeliveryCrewsView(generics.ListAPIView, generics.UpdateAPIView):
    queryset = User.objects.filter(groups__name="Delivery Crew")
    serializer_class = UserWithGroupsSerializer
    permission_classes = [ManagersOnlyPermission]

    def update(self, request, *args, **kwargs):
        username = request.data["username"]
        if username:
            user = get_object_or_404(User, username=username)
            delivery_crew = Group.objects.get(name="Delivery Crew")
            delivery_crew.user_set.add(user)

            return response.Response({"message": "User added to Delivery Crew group."}, status.HTTP_201_CREATED)

        return response.Response({"message": "Failed to add User to Delivery Crew group."}, status.HTTP_400_BAD_REQUEST)


class GroupDeliveryCrewView(generics.RetrieveDestroyAPIView):
    queryset = User.objects.filter(groups__name="Delivery Crew")
    serializer_class = UserWithGroupsSerializer
    permission_classes = [ManagersOnlyPermission]

    def delete(self, request, pk=None, *args, **kwargs):
        if pk:
            user = get_object_or_404(User, id=pk, groups__name="Delivery Crew")
            delivery_crew = Group.objects.get(name="Manager")
            delivery_crew.user_set.remove(user)

            return response.Response({"message": "User removed from Delivery Crew group."})

        return response.Response({"message": "Failed to remove User from Delivery Crew group."}, status.HTTP_400_BAD_REQUEST)
