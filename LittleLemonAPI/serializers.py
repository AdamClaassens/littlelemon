from django.contrib.auth.models import Group
from rest_framework import serializers
from rest_framework.authtoken.admin import User

from .models import Category, MenuItem, Cart, Order, OrderItem


class UserWithGroupsSerializer(serializers.ModelSerializer):
    groups = serializers.SlugRelatedField(
        many=True,
        read_only=True,
        slug_field='name',
    )
    class Meta():
        model = User
        fields = ['id', 'username', 'groups']


class CategorySerializer(serializers.ModelSerializer):
    class Meta():
        model = Category
        fields = ['id', 'title']


class MenuItemSerializer(serializers.ModelSerializer):
    category_id = serializers.IntegerField(write_only=True)
    category = CategorySerializer(read_only=True)
    class Meta():
        model = MenuItem
        fields = ['id', 'title', 'price', 'featured', 'category', 'category_id']


class CartSerializer(serializers.ModelSerializer):
    price = serializers.DecimalField(max_digits=6, decimal_places=2, read_only=True)
    unit_price = serializers.DecimalField(max_digits=6, decimal_places=2, read_only=True)
    class Meta():
        model = Cart
        fields = ['id', 'user', 'menuitem', 'quantity', 'unit_price', 'price']


class OrderSerializer(serializers.ModelSerializer):
    class Meta():
        model = Order
        fields = [ 'id', 'user', 'delivery_crew', 'status', 'total', 'date']


class OrderItemSerializer(serializers.ModelSerializer):
    order_id = serializers.IntegerField(write_only=True)
    order = OrderSerializer(read_only=True)
    class Meta():
        model = OrderItem
        fields = ['id', 'order', 'order_id', 'menuitem', 'quantity', 'unit_price', 'price']
