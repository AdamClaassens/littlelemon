from django.urls import path

from .views import CategoryView, MenuItemsView, MenuItemView, CartView, OrdersView, OrderView, GroupManagersView, GroupManagerView, GroupDeliveryCrewsView, GroupDeliveryCrewView

urlpatterns = [
    path('category/', CategoryView.as_view()),
    path('cetgory/<int:pk>', CategoryView.as_view()),
    path('menu-items/', MenuItemsView.as_view()),
    path('menu-items/<int:pk>', MenuItemView.as_view()),
    path('cart/', CartView.as_view()),
    path('cart/menu-items/', CartView.as_view()),
    path('orders/', OrdersView.as_view()),
    path('orders/<int:pk>', OrderView.as_view()),
    path('groups/manager/users/', GroupManagersView.as_view()),
    path('groups/manager/users/<int:pk>', GroupManagerView.as_view()),
    path('groups/delivery-crew/users/', GroupDeliveryCrewsView.as_view()),
    path('groups/delivery-crew/users/<int:pk>', GroupDeliveryCrewView.as_view()),
]